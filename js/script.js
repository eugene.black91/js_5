// 1.Метод - это функция, которая хранится в свойстве объекта
// 2.Любым типом данных
// 3.Имеется ввиду, что когда мы присваиваем объект в переменную, то переменная не хранит сам объект, а лишь ссылку на него.

function createNewUser() {
  const newUser = {
    firstName: prompt("Please, enter your name"),
    lastName: prompt("Please, enter your last name"),
    getLogin() {
      return (this.firstName.charAt(0) + this.lastName).toLowerCase();
    },
  };
  return newUser;
}
const user = createNewUser();
console.log(user.getLogin());

//Второй вариант    не работает getLogin()

// function createNewUser() {
//   const newUser = {};

//   Object.defineProperties(newUser, {
//     firstName: {
//       value: prompt("Please, enter your name"),
//       writable: false,
//     },
//     lastName: {
//       value: prompt("Please, enter your last name"),
//       writable: false,
//     },
//     getLogin() {
//       return (this.firstName.charAt(0) + this.lastName).toLowerCase();
//     },
//   });

//   return newUser;
// }

// const user = createNewUser();
// console.log(user);
// console.log(user.getLogin());
